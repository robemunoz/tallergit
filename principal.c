#include <stdio.h>
#include <stdlib.h>
int calculaCuadrado(int);
int calculaRectangulo(int, int);
int calculaTriangulo(int, int);

int main(){
	int base, altura, radio,area, opcion; 			//Declaramos las Variables a utilizar
	printf("***Ejemplo de Funciones***\n");			//Imprimimos lo que queremos que salga en pantalla
	printf("1. Calcular �rea de un Cuadrado\n");
	printf("2. Calcular �rea de un Rectangulo\n");
	printf("3. Calcular el �rea de un Triangulo\n");
	printf ("\n Seleccione alguna de las 3 opciones: ");
	scanf("%d", &opcion); //Capturamos la selecci�n   	//Capturamos alguna de las 3 opciones, y la almacenamos en la variable opcion
	if (opcion==1){   					//Realizamos la primera comparaci�n, 
		printf("\n Ingrese el valor de la base del Cuadrado: ");
		scanf("%d",&base);				//Capturamos el valor de la base, y la almacenamos en la variable base	
		area=calculaCuadrado(base);			// Llamamoss a la funcion calculaCuadrado, y le enviamos el valor de la base capturada
		printf("\n El �rea del cuadrado es: %d   \n",area);
	}       
	else  if (opcion==2){
                printf("\n Ingrese el valor de la base del Rectangulo: ");
                scanf("%d",&base);
		printf("\n Ingrese el valor de la altura del Rectangulo: ");
                scanf("%d",&altura);
                area=calculaRectangulo(base,altura);
                printf("\n El �rea del rectangulo es: %d   \n",area);
        }

  	  else  if (opcion==3){
                printf("\n Ingrese el valor de la base del Triangulo: ");
                scanf("%d",&base);
                printf("\n Ingrese el valor de la altura del Triangulo: ");
                scanf("%d",&altura);
                area=calculaTriangulo(base,altura);
                printf("\n El �rea del triangulo  es: %d   \n",area);
        }else {
		printf("\n Opcion no v�lida");
	}
	return (EXIT_SUCCESS);
}
