#como compilar, forma 1:

#compilado y enlazado en una linea

#gcc -o principal principal.c calculaRectangulo.c calculaCuadrado.c calculaTriangulo.c

Forma 2:  compilado:
#gcc -c principal.c
gcc -c calculaRectangulo.c
gcc -c calculaCuadrado.c
gcc -c calculaTriangulo.c

Enlazado

gcc -o principal principal.o calculaRectangulo.o calculaCuadrado.o calculaTriangulo.o

Noten la diferencia.


Para ambos casos, para ejecular es ./principal
